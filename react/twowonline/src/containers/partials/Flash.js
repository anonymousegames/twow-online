import React, {Component, Fragment} from "react";

import {AuthContext} from "../../components/Contexts";


class FlashBody extends Component {
    constructor(props) {
        super(props);

        this.state = {
            displayed: true
        };

        //setTimeout(this.close.bind(this), 5000);
    }

    close() {
        this.props.unmountMe();
    }

    render() {
        return (
            <div className="flash">{this.props.message}
                <div className="flash-close" onClick={this.close.bind(this)}>&#x00d7;</div>
            </div>
        );
    }
}

class Flash extends Component {
    constructor(props) {
        super(props);
        this.state = {renderChild: true};
        this.handleChildUnmount = this.handleChildUnmount.bind(this);
    }

    handleChildUnmount(props) {
        return () => {
            props.authScope.popNotif(props.msgKey);
            this.setState({renderChild: false});
        }
    }

    render() {
        return (
            <Fragment>
                {this.state.renderChild ?
                 <FlashBody unmountMe={this.handleChildUnmount(this.props)} message={this.props.message} /> : null}
            </Fragment>
        );
    }
}


export default class Flashes extends Component {
    constructor(props) {
        super(props);

        this.count = 0;
    }

    loadMessages(auth) {
        let messages = [];
        let msgs = Object.keys(auth.messages);
        msgs.forEach(key => {
            if (auth.messages[key]) {
                messages.push(<Flash key={this.count} msgKey={key} authScope={auth} message={auth.messages[key]}/>);
                this.count++;
            }
        });
        return messages
    }

    render() {
        return (
            <AuthContext.Consumer>
                {auth => {
                    return this.loadMessages(auth)
                }}
            </AuthContext.Consumer>
        );
    }
};
