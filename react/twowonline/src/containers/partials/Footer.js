import React, {Component} from "react";
import {Link} from "react-router-dom";

export default class Footer extends Component {
    render() {
        return (
            <div id="footer">
                <div className="footer-int">
                    <div className="footer-col">
                        <div className="footer-col-head">TWOW Online</div>
                        <p><Link to="/about">About</Link></p>
                        <p><Link to="/terms">Terms of Use</Link></p>
                        <p><Link to="/privacy">Privacy Policy</Link></p>
                    </div>
                    <div className="footer-col">
                        <div className="footer-col-head">Community</div>
                        <p><a href="https://discord.gg/Qct6pAM">HTwins Central</a></p>
                        <p><a href="https://www.youtube.com/user/carykh">@carykh on YouTube</a></p>
                    </div>
                    <div className="footer-col fc-smol">
                        <p>Copyright © 2018 TWOW Online team</p>
                    </div>
                </div>
            </div>
        );
    }
}
