import React, {Component, Fragment} from "react";
import {Link} from "react-router-dom";

import {AuthContext} from "../../components/Contexts";
import NotificationDropdown from "./Notifications";

import {APIContext} from "../../components/API";


class Navbar extends Component {
    constructor(params) {
        super(params);

        this.state = {
            notifications_shown: false,
        };
    }

    toggleNotifications = () => {
        this.setState({notifications_shown: !this.state.notifications_shown});
    };

    render() {
        return (
            <AuthContext.Consumer>{auth =>
                <APIContext.Consumer>{api =>
                    <div className="header">
                        {api.authenticated
                            ? <div onClick={auth.toggleSidebar}
                                   className={"header-burger" + (auth.sidebarShown ? " active" : "")}>
                                <div className="burger-bar1"/>
                                <div className="burger-bar2"/>
                                <div className="burger-bar3"/>
                            </div>
                            : null}
                        <div className="header-brand"><Link to="/">TWOW Online</Link></div>

                        <div className="flex-g"/>

                        {api.authenticated
                            ? <Fragment>
                                {this.state.notifications_shown ?
                                    <NotificationDropdown auth={auth}/> : null}

                                <div onClick={this.toggleNotifications} className="header-lnk">Notifications
                                    {auth.notifications.length > 0 ? "(" + auth.notifications.length + ")" : null}
                                </div>
                                <div className="header-lnk"><Link to="/me">{api.user.username}</Link></div>

                                <div className="header-btns">
                                    <div className="header-btn" onClick={auth.logout.bind(auth, api)}>Log Out</div>
                                </div>
                            </Fragment>
                            : <div className="header-btns">
                                <div className="header-btn"><Link to="/register">Sign Up</Link></div>
                                <div className="header-btn"><Link to="/login">Log In</Link></div>
                            </div>
                        }
                    </div>
                }</APIContext.Consumer>
            }</AuthContext.Consumer>
        );
    }
}

export {Navbar};
