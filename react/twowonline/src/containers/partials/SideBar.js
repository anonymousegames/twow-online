import React, {Component} from "react";
import {Link} from "react-router-dom";
import Footer from './Footer'
import {APIContext} from "../../components/API"
import {AuthContext} from "../../components/Contexts";

import "../../static/css/sidebar.css"


export default class SideBar extends Component {
    nav_lists = {
        "Home": ["/", null],
        "Create a TWOW": ["/new", null],
        "Manage a TWOW": ["/manage", null],
        "Join a TWOW": ["/browse", null],
        "Profile": ["", {
            "My Profile": ["/me", null],
            "Settings": ["/settings", null],
            "My Plugins": ["/my_plugins", null],
            "Logout": ["/logout", null],
        }]
    };

    render() {
        let n = 0;

        function generate(listing) {
            let nav = [];
            Object.keys(listing).forEach(key => {
                n++;
                if (listing[key][1])
                    nav.push(<div key={n}>
                        <li>{key}{generate(listing[key][1])}</li>
                    </div>);
                else
                    nav.push(<Link key={n} to={listing[key][0]}>
                        <li>{key}</li>
                    </Link>);
            });
            n++;
            return <ul key={n} className="sb-nav">{nav}</ul>
        }


        return (
            <AuthContext.Consumer>{auth =>
                <APIContext.Consumer>{api =>
                    <div id="sidebar" className={auth.sidebarShown ? "" : "sidebar-hidden"}>
                        <p>Welcome back, {api.user.username}.</p>
                        {generate(this.nav_lists)}


                        <div className="flex-g"/>
                        <div className="hr"/>

                        <Footer/>
                    </div>
                }</APIContext.Consumer>
            }</AuthContext.Consumer>
        );
    }
}