import React, {Component} from "react";


class Notification extends Component {
    render() {
        return (
            <div className="ndd-item">
                <div className="ndd-item-title">{this.props.title}</div>
                <div className="ndd-item-content">{this.props.content}</div>
                <div onClick={this.props.dismiss(this)} className="ndd-item-action">Dismiss</div>
            </div>
        )
    }
}


export default class NotificationDropdown extends Component {
    dismiss = (notification) => {
        return () => {
            this.props.auth.popNotification(notification.props.index);
        }
    };

    render() {
        let notifications = [];
        this.props.auth.notifications.forEach(item => {
            notifications.push(<Notification index={notifications.length} key={notifications.length}
                                             dismiss={this.dismiss} title={item[0]} content={item[1]}/>);
        });

        return (
            <div className="ndd-body">
                <div className="ndd-heading">Recent Notifications</div>
                <div className="ndd-notifications">
                    {notifications.length > 0 ? notifications : <div className="ndd-text">No recent notifications</div>}
                </div>
            </div>
        )
    }
}
