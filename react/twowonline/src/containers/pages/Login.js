import React, {Component} from "react";
import "../../static/css/auth.css";

import {AuthContext} from "../../components/Contexts";
import {APIContext} from "../../components/API"


export default class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: "",
            message: "",
        };
    }

    validateForm() {
        return this.state.username.length > 0 && this.state.password.length > 0;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    };

    handleSubmit = (auth, api) => {
        return event => {
            event.preventDefault();

            api.login(
                {
                    username: this.state.username,
                    password: this.state.password
                }, (data) => {
                    auth.pushNotif(data.message);
                }, (data) => {
                    this.setState({message: data.error || "Something went wrong!"})
                }, (error) => {
                    this.setState({message: "Something went wrong communicating with the API. Please report this!"});
                    auth.pushNotif(error.toString());
                }
            );
        }
    };

    render() {
        return (
            <AuthContext.Consumer>{auth =>
                <APIContext.Consumer>{api =>
                    <div id="content" className="dark-bg Auth">
                        <form className="auth-modal" onSubmit={this.handleSubmit(auth, api)}>
                            <div className="modal-title">Login</div>

                            <input className="field" name="username" id="username" placeholder="Username"
                                   value={this.state.username}
                                   onChange={this.handleChange}/>
                            <input className="field" type="password" name="password" id="password"
                                   placeholder="Password"
                                   value={this.state.password} onChange={this.handleChange}/>

                            {this.state.message ? <div className="auth-err">{this.state.message}</div> : null}

                            <input className="btn" type="submit" value="Login" disabled={!this.validateForm()}/>
                        </form>
                    </div>
                }</APIContext.Consumer>
            }</AuthContext.Consumer>
        );
    }
}

