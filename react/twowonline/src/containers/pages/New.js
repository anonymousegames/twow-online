import React, {Component} from "react";
import "../../static/css/mp_list.css";

import {ProfileSection} from "../../components/MP"
import AppPage from "../../components/AppPage";

import {APIContext} from "../../components/API";

import "../../static/css/new.css";


export default class NewTwow extends Component {
    render() {
        return (
            <APIContext.Consumer>{api =>
                <AppPage name="New">
                    <div className="new-sect-head">Create a new TWOW</div>
                    <div className="mp-sect">
                        <ProfileSection noCollapse hasImage fields={{
                            name: "",
                            description: "",
                            bio: api.user.bio,
                            css: api.user.styles,
                        }} save_action={api.create_twow} names={{
                            name: "TWOW Name",
                            description: "Brief Description",
                        }} confirm={"This will create a new TWOW"} saveLabel={"Create"} large={["description"]}
                                        lengths={{name: 48, description: 1200}}/>
                    </div>
                </AppPage>
            }</APIContext.Consumer>
        )
    }
}
