import React, {Component} from "react";
import "../../static/css/mp_list.css";

import {ProfileSection, NO_ACTION, EDIT} from "../../components/MP"
import AppPage from "../../components/AppPage";

import {NO_BIO} from "../../components/PlaceHolders"
import {APIContext} from "../../components/API";

import "../../static/css/profile.css";


export class Profile extends Component {
    render() {
        return (
            <AppPage name="Profile">
                <div className="profile-header">
                    <div className="profile-pfp"/>
                    <div>
                        <div className="profile-username">{this.props.owner.username}</div>
                        <div className="profile-sub">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
                    </div>
                </div>

                <ProfileSection fields={{bio: this.props.owner.bio || NO_BIO}} action={NO_ACTION}/>

                <div className="mp-sect-head">{this.props.owner.username}'s TWOWs</div>

                <ProfileSection hasImage fields={{
                    name: "Botter's TLOW",
                    description: "Lol rip",
                    people: "23 Members | 69 Watching"
                }}/>
                <ProfileSection hasImage fields={{
                    name: "Botter's TLOW",
                    description: "Lol rip",
                    people: "23 Members | 69 Watching"
                }}/>
            </AppPage>
        )
    }
}


export class MyProfile extends Component {
    render() {
        return (
            <APIContext.Consumer>{api =>
                <Profile owner={api.user}/>
            }</APIContext.Consumer>
        )
    }
}


export class ProfileSettings extends Component {
    render() {
        return (
            <APIContext.Consumer>{api =>
                <AppPage name="Profile">
                    <div className="mp-sect">
                        <div className="mp-sect-head">Profile settings</div>
                        <ProfileSection hasImage fields={{
                            old_username: api.user.username,
                            username: api.user.username,
                            email: api.user.email,
                            bio: api.user.bio,
                            css: api.user.styles,
                        }} save_action={api.update_user} names={{
                            username: "Username",
                            email: "Email",
                            bio: "Bio",
                            css: "Custom CSS",
                        }}
                                        confirm="Changing these could cause problems for you if you're not sure what you're doing"
                                        large={["bio", "css"]} lengths={{username: 32, bio: 1200, email: 100}}
                                        action={EDIT}/>
                    </div>
                    <div className="mp-sect">
                        <div className="mp-sect-head">Notification settings</div>
                        <ProfileSection fields={{
                            critical_emails: true,
                            promotional_emails: true,
                            in_site_promotions: true,
                        }} names={{
                            critical_emails: "Critical emails",
                            promotional_emails: "Promotional emails",
                            in_site_promotions: "In-site promotions",
                        }} large={["bio", "css"]} lengths={{username: 32, bio: 1200, email: 100}} action={EDIT}/>
                    </div>
                </AppPage>
            }</APIContext.Consumer>
        )
    }
}
