import React from "react";
import {Link} from "react-router-dom";
import "../../static/css/error.css";
import AppPage from "../../components/AppPage";

export default () =>
    <AppPage name="NotFound">
        <div className="nf-wrapper">
            <div className="errorDetails">Nothing's here. Yet.</div>
            <div className="errorExtra">This page is currently being constructed. Check back later and there might just
                be something here.
            </div>
            <Link to="/">
                <div className="err-btn">Get me out of here</div>
            </Link>
        </div>
    </AppPage>;
