import React, {Component} from "react";
import AppPage from "../../components/AppPage";


class DashNavLink extends Component {
    constructor(props) {
        super(props);

        this.state = {
            active: false,
        };
    }

    setActive(active) {
        this.setState({active: active});
    }

    onClick(e) {
        this.setActive(true);
        this.props.moveSlider(e)
    }

    render() {
        return (
            <div onClick={this.onClick.bind(this)}
                 className={"dash-link" + (this.state.active ? " active" : null)}>{this.props.title}</div>
        );
    }
}


export default class TestDash extends Component {
    constructor(props) {
        super(props);

        this.state = {
            slider_styles: {
                left: 0,
                width: 0,
            }
        };
    }

    moveSlider(e) {
        this.setState({
            slider_styles: {
                left: e.target.offsetLeft,
                width: e.target.offsetWidth,
            }
        });
    }

    render() {
        return (
            <AppPage>
                <div id="dash-nav">
                    <div style={this.state.slider_styles} className="dash-slider"/>

                    <DashNavLink title="Overview" moveSlider={this.moveSlider.bind(this)}/>
                    <DashNavLink title="My TWOWs" moveSlider={this.moveSlider.bind(this)}/>
                    <DashNavLink title="Responses" moveSlider={this.moveSlider.bind(this)}/>
                </div>
                <p>Hi</p>
            </AppPage>
        );
    }
}

