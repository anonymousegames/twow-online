import React, {Component} from "react";
import "../../static/css/auth.css";

import {AuthContext} from "../../components/Contexts";
import {APIContext} from "../../components/API";

export default class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: "",
            password2: "",
            email: "",
            message: "",
        };
    }

    validateForm() {
        return this.state.username.length > 0 && this.state.password.length > 0 && this.state.email.length > 0 && this.state.password === this.state.password2;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    };

    handleSubmit = (auth, api) => {
        return event => {
            event.preventDefault();

            api.register(
                {
                    username: this.state.username,
                    password: this.state.password,
                    email: this.state.email
                }, () => {
                    auth.pushNotif("Welcome to TWOW Online!");
                }, (data) => {
                    this.setState({message: data.error || "Something went wrong!"})
                }, (error) => {
                    this.setState({message: "Something went wrong communicating with the API. Please report this!"});
                    auth.pushNotif(error.toString());
                }
            );
        }
    };

    render() {
        return (
            <AuthContext.Consumer>{auth =>
                <APIContext.Consumer>{api =>
                    <div id="content" className="dark-bg Auth">
                        <form className="auth-modal" onSubmit={this.handleSubmit(auth, api)}>

                            <div className="modal-title">Sign Up</div>

                            <input className="field" id="username" placeholder="Username" value={this.state.username}
                                   onChange={this.handleChange}/>
                            <input className="field" id="email" placeholder="Email" value={this.state.email}
                                   onChange={this.handleChange}/>
                            <input className="field" type="password" id="password" value={this.state.password}
                                   placeholder="Password" onChange={this.handleChange}/>
                            <input className="field" type="password" id="password2"
                                   placeholder="Repeat password" value={this.state.password2}
                                   onChange={this.handleChange}/>

                            {this.state.message ? <div className="auth-err">{this.state.message}</div> : null}

                            <input className="btn" type="submit" value="Sign Up" disabled={!this.validateForm()}/>
                        </form>
                    </div>
                }</APIContext.Consumer>
            }</AuthContext.Consumer>
        );
    }
}

