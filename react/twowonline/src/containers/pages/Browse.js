import ProgressiveImage from "react-progressive-image-loading";
import React, {Component} from "react";
import queryString from "query-string";

import "../../static/css/browse.css"

import AppPage from "../../components/AppPage";
import {APIContext} from "../../components/API";
import {Link} from "react-router-dom";


class Card extends Component {
    render() {
        return (
            <div className="browse-card">
                <ProgressiveImage
                    preview={this.props.imageSm}
                    src={this.props.image}
                    transitionTime={150}
                    transitionFunction={"ease"}
                    render={(src, style) => <div className="bc-img"
                                                 style={Object.assign(style, {backgroundImage: `url(${src})`})}/>}
                />

                <div className="card-bottom">
                    <div className="bc-ttl">{this.props.name}</div>
                    <div className="bcb-b">
                        <p className="bcb-sml">{this.props.state} · {this.props.members}</p>
                        <p className="bcb-sml"><Link to={"/profile/" + this.props.ownerId}>{this.props.owner}</Link></p>
                    </div>
                </div>
            </div>
        );
    }
}


class SkeletonCard extends Component {
    render() {
        return (
            <div className="browse-card skeleton">
                <div className="bc-img skeleton">
                    <div className="loader"/>
                </div>


                <div className="card-bottom skeleton">
                    <div className="bc-ttl skeleton"/>
                    <div className="bcb-b">
                        <p className="bcb-sml skeleton"/>
                        <p className="bcb-sml skeleton"/>
                    </div>
                </div>
            </div>
        )
    }
}


class CardList extends Component {
    render() {
        let cardNodes = this.props.data.map((card, index) => {
            return <Card name={card.name} members={card.members} owner={card.owner} ownerId={card.ownerId}
                         state={card.state} image={card.image} imageSm={card.imageSm} key={index}/>;
        });

        return (
            <div id="browse-cards" className="cards-list">
                {cardNodes}
            </div>
        )
    }
}


class SkeletonList extends Component {
    render() {
        let cardNodes = [];
        for (let i = 0; i < this.props.count; i++) {
            cardNodes.push(<SkeletonCard key={i}/>);
        }

        return (
            <div id="browse-cards" className="cards-list">
                {cardNodes}
            </div>
        )
    }
}


class Paginate extends Component {
    getPageElement(index) {
        if (index === this.props.currentPage) {
            return (
                <li key={index} className="active">
                    <a>{index + 1}</a>
                </li>
            )
        } else {
            return (
                <li key={index} onClick={this.props.onPageChange(index)}>
                    <Link to={"?page=" + (index + 1)}>{index + 1}</Link>
                </li>
            )
        }
    }

    render() {
        let links = [];
        const {
            pageRangeDisplayed,
            pageCount,
            marginPagesDisplayed,
            currentPage,
        } = this.props;

        if (pageCount <= pageRangeDisplayed) {
            for (let index = 0; index < pageCount; index++) {
                links.push(this.getPageElement(index));
            }
        } else {

            let leftSide = (pageRangeDisplayed / 2);
            let rightSide = (pageRangeDisplayed - leftSide);

            if (currentPage > pageCount - pageRangeDisplayed / 2) {
                rightSide = pageCount - currentPage;
                leftSide = pageRangeDisplayed - rightSide;
            }
            else if (currentPage < pageRangeDisplayed / 2) {
                leftSide = currentPage;
                rightSide = pageRangeDisplayed - leftSide;
            }

            let index;
            let page;
            let breakView = null;
            let createPageView = (index) => this.getPageElement(index);

            for (index = 0; index < pageCount; index++) {

                page = index + 1;

                if (page <= marginPagesDisplayed) {
                    links.push(createPageView(index));
                    continue;
                }

                if (page > pageCount - marginPagesDisplayed) {
                    links.push(createPageView(index));
                    continue;
                }

                if ((index >= currentPage - leftSide) && (index <= currentPage + rightSide)) {
                    links.push(createPageView(index));
                    continue;
                }

                if (links[links.length - 1] !== breakView) {
                    breakView = (
                        <li key={index}><a>...</a></li>
                    );
                    links.push(breakView);
                }
            }
        }

        return (
            <ul className="pagination">
                {links}
            </ul>
        )
    }
}


export default class TWOWBrowse extends Component {
    perPage = 15;

    constructor(props) {
        super(props);

        let parsed = queryString.parse(props.routeProps.location.search);
        let offset;
        try {
            offset = parseInt(parsed.page, 10) - 1;
            if (!offset) offset = 0;
        } catch (e) {
            offset = 0;
        }
        offset = Math.max(offset, 0);

        console.log(offset);

        this.state = {
            data: [],
            offset: offset,
            skeleton: true,
        };

        this.api = null;
    }

    loadCardsFromServer() {
        this.api.search_twows({
            perPage: this.perPage,
            page: this.state.offset,
        }, (data) => {
            this.setState({
                data: data.page,
                pageCount: Math.ceil(data.resultsFound / this.perPage),
                skeleton: false,
            });
        }, () => {
        });
    };

    componentDidMount() {
        this.loadCardsFromServer();
    }

    handlePageClick = (index) => {
        return () => {
            this.setState({offset: index, skeleton: true}, () => {
                this.loadCardsFromServer();
            });
        }
    };

    registerAPI = (api) => {
        this.api = api;
    };

    render() {
        return (
            <AppPage name="Browse">
                <APIContext.Consumer>
                    {api => {
                        this.registerAPI(api)
                    }}
                </APIContext.Consumer>

                <div className="browse-wrapper">
                    {this.state.skeleton
                        ? <SkeletonList count={this.perPage}/>
                        : <CardList data={this.state.data}/>
                    }

                    <Paginate
                        pageCount={this.state.pageCount}
                        currentPage={this.state.offset}
                        onPageChange={this.handlePageClick}
                        marginPagesDisplayed={2}
                        pageRangeDisplayed={4}
                    />
                </div>
            </AppPage>
        );
    }
}

