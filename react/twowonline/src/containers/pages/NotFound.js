import React from "react";
import {Link} from "react-router-dom";
import "../../static/css/error.css";
import AppPage from "../../components/AppPage";

export default () =>
    <AppPage name="NotFound">
        <div className="nf-wrapper">
            <div className="errorDetails">Page Not Found</div>
            <div className="errorExtra">The page you are looking for was moved, removed, renamed or never existed.</div>
            <Link to="/"><div className="err-btn">Get me out of here</div></Link>
        </div>
    </AppPage>;
