import React from "react";
import {Switch, Route, Redirect} from "react-router-dom";

import { AuthContext } from "./Contexts";

import {MyProfile, ProfileSettings} from "../containers/pages/Profile";
import {TWOWBrowse, BrowseMine} from "../containers/pages/Browse";
import TestDash from "../containers/pages/TestDash";
import NotFound from "../containers/pages/NotFound";
import Register from "../containers/pages/Register";
import NewTwow from "../containers/pages/New";
import Login from "../containers/pages/Login";
import {APIContext} from "../components/API";
import WIP from "../containers/pages/WIP";


function loggedIn(api) {
    return api.authenticated;
}

function checkAuth(C, api, needed=true) {
    return (props) => {
        return (
            (needed ^ loggedIn(api)) ? (
                <Redirect to={needed ? "/login" : "/"}/>
            ) : (
                <C routeProps={props}/>
            )
        )
    }
}
function logout(auth, api) {
    return () => {
        console.log("Logout");
        auth.logout(api);
        return <Redirect to="/"/>;
    }
}

export default () =>
    <AuthContext.Consumer>{auth =>
        <APIContext.Consumer>{api =>
            <Switch>
                <Route path="/" exact render={checkAuth(TestDash, api)}/>
                <Route path="/login" exact render={checkAuth(Login, api, false)}/>
                <Route path="/register" exact render={checkAuth(Register, api, false)}/>

                <Route path="/new" exact render={checkAuth(NewTwow, api)}/>
                <Route path="/join" exact render={checkAuth(WIP, api)}/>
                <Route path="/browse" exact render={checkAuth(TWOWBrowse, api)}/>
                <Route path="/manage" exact render={checkAuth(BrowseMine, api)}/>
                <Route path="/settings" exact render={checkAuth(ProfileSettings, api)}/>
                <Route path="/me" exact render={checkAuth(MyProfile, api)}/>
                <Route path="/my_plugins" exact render={checkAuth(WIP, api)}/>
                <Route path="/logout" exact render={logout(auth, api)}/>


                <Route component={NotFound}/>
            </Switch>
        }</APIContext.Consumer>
    }</AuthContext.Consumer>