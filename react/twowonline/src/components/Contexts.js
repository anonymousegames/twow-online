import React from "react";

export const AuthContext = React.createContext({
    setLogin: () => {},
    username: "",
    email: "",
    bio: "",
    messages: [],
    logout: () => {},
    pushNotif: () => {},
    popNotif: () => {},
    notifications: [],
    popNotification: () => {},
});
