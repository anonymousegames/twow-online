import React from "react";


export default () =>
    <div className="loading-spinner">
        <div className="loading-internal">
            <div className="loader">Loading...</div>

            <div className="loading-text">We're logging you in...</div>
        </div>
    </div>
