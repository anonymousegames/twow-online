import React, {Component} from "react";
import SideBar from "../containers/partials/SideBar";
import {APIContext} from "./API";

import "../static/css/dash.css";


export default class AppPage extends Component {
    render() {
        return (
            <APIContext.Consumer>{api =>
                <div id="content">
                    {api.authenticated ? <SideBar/> : null}
                    <div className={"content-int " + (this.props.name || "")}>
                        {this.props.children}
                    </div>
                </div>
            }</APIContext.Consumer>
        );
    }
}

