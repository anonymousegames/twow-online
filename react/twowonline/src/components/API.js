import {withRouter} from "react-router-dom";
import React, {Component} from "react";
import axios from "axios";


export const APIContext = React.createContext({
    ready: false,
    authenticated: false,
    user: {
        username: null,
        email: null,
        bio: null,
        css: null,
        id: null,
    },
});


class APIClass extends Component {
    PROTOCOL = "http:";
    DOMAIN = "//localhost:5000";
    API_BASE = "/";
    BASE_URL = this.PROTOCOL + this.API_BASE + this.DOMAIN;
    ENDPOINTS = {
        VALIDATE_TOKEN: "/validate_token",
        UPDATE_USER: "/update_user",
        REGISTER: "/register",
        LOGIN: "/login",

        CREATE_TWOW: "/twows/create",
        TWOW_SEARCH: "/twows/search",
    };

    constructor() {
        super();

        this.state = {
            ready: false,
            authenticated: false,
            user: {
                username: null,
                email: null,
                bio: null,
                css: null,
                id: null,
            },

            register: this.register,
            login: this.login,
            logout: this.logout,
            update_user: this.update_user,
            search_twows: this.search_twows,
            create_twow: this.create_twow,
        };
    }

    componentWillMount() {
        let token = localStorage.getItem('token');
        if (token) {
            this.validate_token({token: token}, (data) => {
                this.setState({
                    user: {
                        username: data.username,
                        email: data.email,
                        bio: data.bio,
                        css: data.css,
                        id: token.split(":")[0],
                    },
                    authenticated: true,
                    ready: true,
                });
            }, () => {
                this.setState({
                    authenticated: false,
                    ready: true,
                });
                console.warn("Previous token invalid!")
            }, (error) => {
                this.setState({
                    authenticated: false,
                    ready: true,
                });
                console.error("Failed to query API!\n" + error.toString());
            });
        } else {
            this.setState({
                ready: true,
            });
        }
    }

    search_twows = (data, success, critical) => {
        axios({
            url: this.BASE_URL + this.ENDPOINTS.TWOW_SEARCH,
            method: "get",
            params: {
                perPage: data.perPage,
                page: data.page || 0,
                status: data.status || null,
                owner: data.owner || null,
            }
        }).then(response => {
            success(response.data, this);
        }).catch(critical);
    };

    validate_token = (data, success, failure, critical) => {
        axios({
            url: this.BASE_URL + this.ENDPOINTS.VALIDATE_TOKEN,
            method: "post",
            data: {token: data.token},
        }).then(response => {
            if (response.data.success) success(response.data, this);
            else failure(response.data, this);
        }).catch(critical);
    };

    login = (data, success, failure, critical) => {
        axios({
            url: this.BASE_URL + this.ENDPOINTS.LOGIN,
            method: "post",
            data: {uname: data.username, password: data.password}
        }).then(response => {
            if (response.data.success) {
                localStorage.setItem('token', response.data.token);

                this.setState({
                    authenticated: true,
                    user: {
                        username: data.username,
                        email: response.data.email,
                        bio: response.data.bio,
                        css: response.data.css,
                        id: response.data.token.split(":")[0]
                    }
                });

                success(response.data, this);
            }
            else failure(response.data, this);
        }).catch(critical);
    };

    register = (data, success, failure, critical) => {
        axios({
            url: this.BASE_URL + this.ENDPOINTS.REGISTER,
            method: "post",
            data: {uname: data.username, password: data.password, email: data.email}
        }).then(response => {
            if (response.data.success) {
                localStorage.setItem('token', this.token);

                this.setState({
                    authenticated: true,
                    user: {
                        username: data.username,
                        email: data.email,
                        bio: "",
                        css: "",
                        id: this.token.split(":")[0],
                    }
                });

                success(response.data, this);
            }
            else failure(response.data, this);
        }).catch(critical);
    };

    logout = () => {
        this.setState({
            authenticated: false,
            user: {
                username: null,
                email: null,
                bio: null,
                css: null,
                id: null,
            },
            ready: true,
        });
        localStorage.removeItem('token');
        localStorage.removeItem('sidebarShown');
    };

    update_user = (data, success, failure, critical) => {
        axios({
            url: this.BASE_URL + this.ENDPOINTS.UPDATE_USER,
            method: "post",
            data: {old_uname: data.old_username, uname: data.username, email: data.email, bio: data.bio},
            headers: {
                Authorization: localStorage.getItem('token')
            }
        }).then(response => {
            if (response.data.success) {
                this.setState({
                    user: {
                        username: data.username,
                        email: data.email,
                        bio: data.bio,
                        css: data.css,
                    }
                });

                success(response.data, this);
            }
            else failure(response.data, this);
        }).catch(critical);
    };

    create_twow = (data, success, failure, critical) => {
        axios({
            url: this.BASE_URL + this.ENDPOINTS.CREATE_TWOW,
            method: "post",
            data: {name: data.name, description: data.description, image: data.image},
            headers: {
                Authorization: localStorage.getItem('token')
            }
        }).then(response => {
            if (response.data.success) {
                success(response.data, this);
                this.props.history.push(`/twow/${response.data.id}`);
            }
            else failure(response.data, this);
        }).catch(critical);
    };

    render() {
        return (<APIContext.Provider value={this.state}>{this.props.children}</APIContext.Provider>)
    }
}

export let API = withRouter(APIClass);
