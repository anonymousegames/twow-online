import React, {Component, Fragment} from "react";

import {AuthContext} from "./Contexts";


export const EDIT = "Edit",
    COLLAPSE = "Collapse",
    NO_ACTION = "None",
    EXPAND = "Expand";


class SectionField extends Component {
    constructor(props) {
        super(props);

        this.state = {
            required: props.required || false,
            content: props.content || "",
            editing: props.editing || false,
            hidden: props.hidden || false,
            length: props.length || null,
            value: props.content || "",
            title: props.title || "",
        };
        this.props.onChange(this.props.component, this.state.value);
        this.props.didSaveChange(this.props.component, this.didSaveChange)
    }

    didSaveChange = (state) => {
        if (state)
            this.setState({content: this.state.value});
        else
            this.setState({value: this.state.content});
    };

    componentWillReceiveProps(newProps) {
        this.setState({editing: newProps.editing});
    }

    onChange = event => {
        this.setState({
            value: this.state.length ? event.target.value.substring(0, this.state.length) : event.target.value
        }, () => {
            this.props.onChange(this.props.component, this.state.value);
        });
    };

    render() {
        return (<Fragment>
            {this.state.hidden ? null :
                <div className="mp-prop">
                    <div className="mp-prop-lab">{this.state.title}</div>
                    <p>{this.state.editing}</p>
                    {!this.state.editing ? <div className="mp-prop-val">{this.state.content}</div>
                        : this.props.large ?
                            <textarea className="mp-prop-inp" rows="6" onChange={this.onChange}
                                      value={this.state.value}></textarea>
                            : <input className="mp-prop-inp" onChange={this.onChange} value={this.state.value}/>}
                    {this.state.editing && this.state.length ?
                        <div className="mp-prop-chars">{this.state.value.length}/{this.state.length}</div> : null}
                </div>
            }
        </Fragment>)
    }
}


export class ProfileSection extends Component {
    constructor(props) {
        super(props);

        this.state = {
            collapsed: props.action === COLLAPSE || !props.action,
            noCollapse: props.noCollapse || false,
            saveLabel: props.saveLabel || "Save",
            linkAction: props.action || EXPAND,
            hasImage: props.hasImage || false,
            lengths: props.lengths || {},
            fields: props.fields || {},
            image: props.image || null,
            large: props.large || [],
            editing: false,
        };

        if (this.state.noCollapse) {
            this.state.collapsed = false;
            this.state.linkAction = null;
            this.state.editing = true;
        }

        this.save_action = props.save_action || undefined;
        this.saveCallbacks = {};
        this.fields = {};
        this.auth = {};
    }

    onClick() {
        switch (this.state.linkAction) {
            case EDIT:
                this.setState({editing: true});
                break;
            case COLLAPSE:
                this.setState({collapsed: true, linkAction: EXPAND});
                break;
            case EXPAND:
                this.setState({collapsed: false, linkAction: COLLAPSE});
                break;
            default:
                break;
        }
    }

    cancelEdit = () => {
        this.setState({editing: false});
        Object.keys(this.saveCallbacks).forEach(key => {
            this.saveCallbacks[key](false);
        });
    };

    expand = () => {
        if (this.state.linkAction === EXPAND) {
            this.setState({collapsed: false, linkAction: COLLAPSE});
        }
    };

    confirm = (message, callback) => {
        this.auth.showMessage("Are you sure you want to do this?", message, undefined, true, callback);
    };

    showError = (message, details) => {
        details = details ? details.toString ? details.toString() : details : null;
        this.auth.showMessage("Error performing action", message, details, false);
    };

    childUpdate = (component, value) => {
        this.fields[component] = value;
    };

    doSave = (auth) => {
        let self = this;
        this.auth = auth;

        return () => {
            if (!self.save_action) {
                return self.showError("No save action implemented for this settings pane.", "Might wanna' report this unless you were messing around in the source code.")
            }

            function cb() {
                if (self.save_action) {
                    self.save_action(
                        self.fields,
                        () => {
                            self.setState({editing: false});
                            Object.keys(self.saveCallbacks).forEach(key => {
                                self.saveCallbacks[key](true);
                            });
                        }, (data) => {
                            self.showError(data.error || "Something went wrong!");
                        }, (error) => {
                            self.showError("Something went wrong communicating with the API. Please report this!", error)
                        }
                    );
                }
            }

            if (self.props.confirm)
                self.confirm(self.props.confirm, cb);
            else
                cb();
        }
    };

    bindDidSaveChange = (key, callback) => {
        this.saveCallbacks[key] = callback;
    };

    render() {
        let fields = [];

        let n = 0;
        Object.keys(this.state.fields).forEach(field => {
            let hidden = this.props.names && !this.props.names[field];
            fields.push(<SectionField key={n} required={true} editing={this.state.editing}
                                      component={field} onChange={this.childUpdate}
                                      title={(this.props.names || {})[field] || field}
                                      content={this.state.fields[field]}
                                      didSaveChange={this.bindDidSaveChange}
                                      large={this.state.large.indexOf(field) !== -1}
                                      length={this.state.lengths[field]}
                                      hidden={hidden}/>);
            n++;
        });

        return (
            <AuthContext.Consumer>
                {auth =>
                    <div onClick={this.expand}
                         className={"mp-sect-body" + (this.state.collapsed ? " mp-sect-collapsed" : "")
                         + (this.state.editing ? " mp-sect-editing" : "")}>

                        {this.state.hasImage ?
                            <div style={{backgroundImage: this.state.image}} className="mp-sect-pfp"/>
                            : null}
                        <div className="mp-sect-info">
                            {fields}
                            {this.state.editing ? <Fragment>
                                <div className="hr"/>
                                <div className="mp-sect-btns">
                                    {this.state.noCollapse ? null :
                                        <button onClick={this.cancelEdit} className="mp-sect-btn">Cancel</button>}
                                    <button onClick={this.doSave(auth)}
                                            className="mp-sect-btn btn-l">{this.state.saveLabel}</button>
                                </div>
                            </Fragment> : null}
                        </div>
                        {this.state.linkAction !== NO_ACTION ?
                            <div className="mp-sect-link"
                                 onClick={this.onClick.bind(this)}>{this.state.linkAction}</div> : null}
                    </div>
                }
            </AuthContext.Consumer>
        )
    }
}
