import React, {Component, Fragment} from "react";
import {Navbar} from "../containers/partials/Navbar";
import {Helmet} from "react-helmet";
import "../static/css/common.css";
import "../static/css/mp_list.css";

import Routes from "../components/Routes";

import LoadingSpinner from "../components/LoadingSpinner";
import {AuthContext} from "../components/Contexts";
import Flashes from "../containers/partials/Flash";
import Draggable from 'react-draggable';
import {API, APIContext} from "./API";


class App extends Component {
    notifLimit = 5;

    constructor(props) {
        super(props);

        this.logout = (api) => {
            this.setState({username: ""}, () => {
                api.logout();
                this.pushNotif("You have been successfully logged out!");
            });
        };
        this.pushNotif = (notif) => {
            let new_i = {};
            new_i[Math.random()] = notif;
            Object.assign(this.state.messages, new_i);
            this.setState({messages: new_i});
        };
        this.popNotif = (key) => {
            let new_i = {};
            new_i[key] = undefined;
            Object.assign(this.state.messages, new_i);
            this.setState({messages: new_i});
        };

        this.popNotification = (index) => {
            let new_notifications = [...this.state.notifications];
            new_notifications.splice(index, 1);
            this.setState({
                notifications: new_notifications
            });
        };

        this.clearMessage = (onCancel) => {
            return () => {
                this.setState({message: {shown: false}});
                if (onCancel) onCancel();
            }
        };

        this.showMessage = (title, body, details, canCancel, onContinue, onCancel) => {
            this.setState({
                message: {
                    onContinue: this.clearMessage(onContinue),
                    canCancel: canCancel,
                    onCancel: this.clearMessage(onCancel),
                    details: details,
                    shown: true,
                    title: title,
                    body: body,
                }
            })
        };

        this.state = {
            logout: this.logout,
            popNotification: this.popNotification,
            messages: [],
            notifications: [["Hey there!", "Looks like you're new around these parts. For now, have a play around to work out the site. A built-in tour is coming soon!"],
                ["Data leak", "This is an example message for if we had a GDPR data breach."],
                ["Verify email", "Your email isn't verified. Please verify your email to complete registration!"],
                ["Noah", "He totally sucks cock. Trust me."],
                ["Privacy", "We have updated our privacy policy."]],
            pushNotif: this.pushNotif,
            popNotif: this.popNotif,

            sidebarShown: localStorage.getItem('sidebarShown') !== null
                ? localStorage.getItem('sidebarShown'): true,

            styles: `.ccontent-int {
    background-position: center;
    background-size: cover;
    background-attachment: fixed;
    background-image: linear-gradient(
            rgba(74, 112, 124, 0.7),
            rgba(74, 112, 124, 0.8)
    ), url('https://wallpapersite.com/images/wallpapers/shakugan-no-shana-3840x2160-4k-5095.jpg');
}`,

            message: {
                onContinue: () => {
                },
                onCancel: () => {
                },
                canCancel: false,
                details: "",
                shown: false,
                title: "",
                body: "",
            },
            showMessage: this.showMessage,
            toggleSidebar: () => {
                this.setState({sidebarShown: !this.state.sidebarShown}, () => {
                    localStorage.setItem('sidebarShown', this.state.sidebarShown ? "1" : "");
                })
            },
        };
    }

    render() {
        return (
            <API><APIContext.Consumer>{api =>
                <AuthContext.Provider value={this.state}>
                    <Helmet>
                        <style>
                            {this.state.styles}
                        </style>
                    </Helmet>
                    <div id="app">
                        {api.ready ?
                            <Fragment>
                                <Navbar/>
                                <Flashes/>
                                <Routes/>
                            </Fragment>
                            : <LoadingSpinner/>
                        }
                    </div>

                    {this.state.message.shown ?
                        <div className="darken">
                            <Draggable>
                                <div className="mp-sect-msg">
                                    <div className="mp-sect-msg-title">{this.state.message.title}</div>
                                    <div className="mp-sect-msg-body">{this.state.message.body}</div>
                                    <div className="mp-sect-msg-extra">{this.state.message.details}</div>
                                    <div className="hr"/>
                                    <div className="mp-sect-btns">
                                        {this.state.message.canCancel ?
                                            <button onClick={this.state.message.onCancel}
                                                    className="mp-sect-btn">Cancel</button> : null}
                                        <button onClick={this.state.message.onContinue}
                                                className="mp-sect-btn btn-l">Continue
                                        </button>
                                    </div>
                                </div>
                            </Draggable>
                        </div> : null}
                </AuthContext.Provider>
            }</APIContext.Consumer></API>
        );
    }
}

export default App;
