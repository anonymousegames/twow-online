import psycopg2

from flask import current_app, g


columns = {
    "USERS_ID": 0,
    "USERS_CREATE_DATE": 1,
    "USERS_USERNAME": 2,
    "USERS_PASSWORD": 3,
    "USERS_EMAIL": 4,
    "USERS_BIO": 5,
    "TWOWS_ID": 0,
    "TWOWS_CREATE_DATE": 1,
    "TWOWS_NAME": 2,
    "TWOWS_STATUS": 3,
    "TWOWS_OWNER": 4,
    "PERFORMANCES_CREATE_DATE": 0,
    "PERFORMANCES_USER_ID": 1,
    "PERFORMANCES_TWOW_ID": 2
}


def get_db():
    if "db" not in g:
        pw = open("secrets/postgres_password").read().strip()
        user = open("secrets/postgres_username").read().strip()
        g.db = psycopg2.connect(
            database="twowonline", password=pw, user=user
        ).cursor()

        #init_db()

    return g.db


def close_db(e=None):
    db = g.pop("db", None)

    if db is not None:
        db.close()


def init_db():
    db = get_db()

    with current_app.open_resource("init_db.sql") as f:
        db.execute(f.read().decode("utf8"))
