import json
from flask import request, jsonify, Blueprint, Response, session
from .db import get_db, columns
from .auth import login_required
from werkzeug.security import generate_password_hash

bp = Blueprint("user", __name__, url_prefix="")


@bp.route("/get_user")
def get_user():
    try:
        data = json.loads(request.data)
    except json.JSONDecodeError:
        data = request.form
    error = user = None
    return jsonify(
        uname=user[columns["USERS_USERNAME"]],
        email=user[columns["USERS_EMAIL"]],
        create_date=user[columns["USERS_CREATE_DATE"]]
    )


@bp.route("/update_user", methods=("POST",))
@login_required
def update_user():
    try:
        data = json.loads(request.data)
    except json.JSONDecodeError:
        data = request.form

    db = get_db()

    error = None
    old_username = data.get("old_uname")
    return_data = {}

    db.execute("SELECT * FROM users WHERE username=%s", (old_username,))
    user = db.fetchone()
    if not user:
        error = "Unknown user!"
    else:
        username = data.get("uname", user[columns["USERS_USERNAME"]])
        password = data.get("password")
        email = data.get("email", user[columns["USERS_EMAIL"]])
        bio = data.get("bio", user[columns["USERS_BIO"]])

        if error is not None:
            session.clear()
        else:
            params = [email, username, bio]
            params_str = "SET email = %s, username = %s, bio = %s"
            if password is not None:
                params.append(generate_password_hash(password))
                params_str += f" password = %s"
            params.append(old_username)

            cmd = f"UPDATE users {params_str} WHERE username=%s"
            db.execute(cmd, params)
            db.connection.commit()

            return_data["username"] = username
            return_data["email"] = email
            return_data["bio"] = bio

    return jsonify(
        success=not error,
        error=error,
        **return_data
    )
