CREATE TABLE IF NOT EXISTS users (
    id SERIAL PRIMARY KEY,
    create_date TIMESTAMP,
    username TEXT UNIQUE NOT NULL,
    password TEXT NOT NULL,
    email TEXT NOT NULL,
    bio TEXT
);

CREATE TABLE IF NOT EXISTS twows (
    id SERIAL PRIMARY KEY,
    create_date TIMESTAMP,
    name TEXT NOT NULL,
    status INT NOT NULL,
    owner BIGINT
);

CREATE TABLE IF NOT EXISTS performances (
    create_date TIMESTAMP,
    user_id INTEGER,
    twow_id INTEGER
);
