$(function() {
    var target = 6324687,
        time = 1500,
        current = 0,
        counter = $("#twow-count"),
        frame = 50,
        interval;

    interval = setInterval(function() {
        current += target / (time / frame);
        if (current >= target) {
            current = target;
            clearInterval(interval);
        }
        counter.text(Math.round(current));
    }, frame);
});
