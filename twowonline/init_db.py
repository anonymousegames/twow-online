import psycopg2

TABLES = {
    'users': [
        'id SERIAL PRIMARY KEY',
        'create_date TIMESTAMP',
        'username TEXT UNIQUE NOT NULL',
        'password TEXT NOT NULL',
        'email TEXT NOT NULL',
        'bio TEXT'
    ],
    'twows': [
        'id SERIAL PRIMARY KEY',
        'create_date TIMESTAMP',
        'name TEXT NOT NULL',
        'status INT NOT NULL',
        'owner BIGINT'
    ],
    'performances': [
        'create_date TIMESTAMP',
        'user_id INTEGER',
        'twow_id INTEGER'
    ],
}

pw = open("../secrets/postgres_password").read().strip()
user = open("../secrets/postgres_username").read().strip()
db = psycopg2.connect(
    database="twowonline", password=pw, user=user
)


def main():
    cursor = db.cursor()
    for k, v in TABLES.items():
        cursor.execute(f"""CREATE TABLE IF NOT EXISTS {k}()""")
        cursor.execute(f"""SELECT * FROM {k} LIMIT 0""")
        new_columns = [c.split()[0] for c in v]
        old_columns = [c.name for c in cursor.description if c.name in new_columns]
        print(cursor.description)
        for n, o in zip(new_columns, old_columns):
            if n != o: break
        else:
            if len(cursor.description) == len(new_columns):
                continue

        tmp = k + 'tmp'
        columns = ', '.join(v)
        print(columns)
        cursor.execute(f"""
            ALTER TABLE {k} RENAME TO {tmp};
            CREATE TABLE {k}({columns});
        """)
        if old_columns:
            old_columns = ', '.join(old_columns)
            cursor.execute(f"""INSERT INTO {k} ({old_columns}) SELECT {old_columns} FROM {tmp}""")
        cursor.execute(f"""DROP TABLE {tmp}""")

    db.commit()


if __name__ == '__main__':
    main()
