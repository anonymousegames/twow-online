import json
from flask import Blueprint, request, render_template, jsonify

from twowonline.db import get_db, columns

bp = Blueprint("twows", __name__, url_prefix="/twows")

STATES = {
    99: 'Currently recruiting',
    0: 'Awaiting prompt',
    1: 'Waiting for submissions',
    2: 'Voting'
}

@bp.route("/create", methods=("POST",))
def create_twow():
    try:
        data = json.loads(request.data)
    except json.JSONDecodeError:
        data = request.form

    db = get_db()
    error = None

    try:
        name = data["name"]
    except KeyError:
        error = "Name is required!"

    if not error:
        #TODO: Insert owner and status, add login_required when that's done
        db.execute("INSERT INTO twows VALUES (DEFAULT, 'now', %s, 0, 0)", (name,))
        db.connection.commit()

    return jsonify(success=not error, error=error)


@bp.route('/search', methods=('get',))
def search():
    data = request.json
    if data is None:
        data = request.args
    per_page = int(data.get('perPage', 15))
    page = int(data.get('page', 0))
    status = data.get('state', -1)  # Usage: "active,-voting" = active but not in voting
    owner = int(data.get('owner', -1))

    db = get_db()
    db.execute("""
        SELECT * FROM twows 
        WHERE (%(owner)s = -1 OR %(owner)s = owner) 
        AND (%(status)s = -1 OR %(status)s = status)
    """, {'status': status, 'owner': owner})

    raw_twows = db.fetchall()
    found = len(raw_twows)
    raw_twows = raw_twows[page * per_page: (page+1) * per_page]
    twows = []
    for twow in raw_twows:
        twow_result = {
            'name': twow[columns['TWOWS_NAME']],
            'state': STATES[twow[columns['TWOWS_STATUS']]],
            'ownerId': twow[columns['TWOWS_OWNER']],
            #TODO: Implement members and twow images
            'members': 10,
            'imageSm': 'https://en.touhouwiki.net/images/thumb/b/b1/FS_Kosuzu2.png/275px-FS_Kosuzu2.png',
            'image': 'https://en.touhouwiki.net/images/b/b1/FS_Kosuzu2.png'
        }
        db.execute("""SELECT username FROM users WHERE id = %s""", (twow_result['ownerId'],))
        owner = db.fetchone()
        twow_result['owner'] = owner
        twows.append(twow_result)

    return jsonify(
        resultsFound=found,
        page=twows
    )
