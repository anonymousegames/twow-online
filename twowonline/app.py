from flask_cors import CORS
from flask import Flask, render_template
import os


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(SECRET_KEY="dev")
    CORS(app)

    if test_config:
        # load the test config if passed in
        app.config.from_mapping(test_config)
    else:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile("config.py", silent=True)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from . import auth, twows, user
    app.register_blueprint(auth.bp)
    app.register_blueprint(twows.bp)
    app.register_blueprint(user.bp)

    @app.route("/")
    def index_page_placeholder():
        return render_template("index.html")

    @app.route("/doc")
    def doc_page():
        return render_template("doc.html")

    return app
