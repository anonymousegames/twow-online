import functools
import random
import hmac

from flask import (
    Blueprint, g, redirect, render_template, request, session, url_for, jsonify
)

from werkzeug.security import check_password_hash, generate_password_hash
from twowonline.db import get_db, columns
import json

bp = Blueprint("auth", __name__, url_prefix="")

with open('twowonline/messages.txt') as file_:
    login_messages = file_.read().strip().split('\n')


def authorize(token):
    if type(token) == bytes: token = token.decode('utf8')
    uid, token = token.split(':')
    db = get_db()
    db.execute("SELECT * FROM users WHERE id=%s", (int(uid),))
    user = db.fetchone()
    if user is None: raise ValueError
    tester = hmac.new(user[columns['USERS_PASSWORD']].encode('utf-8'), uid.encode('utf-8'))
    return hmac.compare_digest(token, tester.hexdigest())


@bp.route("/register", methods=("POST",))
def register():
    try:
        data = json.loads(request.data)
    except json.JSONDecodeError:
        data = request.form

    username = password = email = None
    db = get_db()
    error = None

    try:
        username = data["uname"]
    except KeyError:
        error = "Username is required!"
    try:
        password = data["password"]
    except KeyError:
        error = "Password is required!"
    try:
        email = data["email"]
    except KeyError:
        error = "Email is required!"
    if not error:
        db.execute("SELECT id FROM users WHERE username = %s", (username,))
        if db.fetchone():
            error = f"User {username} is already registered!"
        else:
            cmd = "INSERT INTO users VALUES (DEFAULT, 'now', %s, %s, %s)"
            db.execute(cmd, (username, generate_password_hash(password),
                             email))
            db.connection.commit()

    # TODO: THIS IS A DUMMY. SOMEONE ON B/E SHOULD DO THIS PROPERLY
    params = str('I don\'t know how to get uids').encode('utf-8')
    token = hmac.new(password.encode('utf-8'), params).hexdigest()
    return jsonify(
        success=not error,
        error=error,
        token=str('I don\'t know how to get uids') + ':' + token,
    )


@bp.route('/validate_token', methods=('POST',))
def validate_token():
    # TODO: THIS IS A DUMMY SO THE FRONT END WORKS. IMPLEMENT THIS!!
    try:
        data = json.loads(request.data)
    except json.JSONDecodeError:
        data = request.form

    return jsonify(
        token=data['token'],
        success=True,
        username='Bottersnike',
        email='email@emai.com',
        bio='fun bio',
    )


@bp.route("/login", methods=("POST",))
def login():
    try:
        data = json.loads(request.data)
    except json.JSONDecodeError:
        data = request.form

    username = password = error = message = email = None
    db = get_db()
    try:
        username = data["uname"]
    except KeyError:
        error = "Username is required!"
    try:
        password = data["password"]
    except KeyError:
        error = "Password is required!"
    db.execute("SELECT * FROM users WHERE username=%s", (username,))
    user = db.fetchone()
    if user is None or not check_password_hash(user[columns["USERS_PASSWORD"]], password):
        error = "Incorrect username or password!"

    if error is not None:
        session.clear()
        return jsonify(success=False, error=error)
    else:
        message = random.choice(login_messages).replace(
            '{USERNAME}', username)
        session["user_id"] = user[columns["USERS_ID"]]
        email = user[columns["USERS_EMAIL"]]

    params = str(user[columns["USERS_ID"]]).encode('utf-8')
    token = hmac.new(user[columns['USERS_PASSWORD']].encode('utf-8'), params).hexdigest()
    return jsonify(
        success=not error,
        error=error,
        message=message,
        email=email,
        bio=user[columns['USERS_BIO']],
        token=str(user[columns['USERS_ID']]) + ':' + token,
    )


@bp.before_app_request
def load_logged_in_user():
    user_id = session.get("user_id")

    if user_id is None:
        g.user = None
    else:
        db = get_db()
        db.execute("SELECT * FROM users WHERE id=%s", (user_id,))
        g.user = db.fetchone()


@bp.route("/logout")
def logout():
    session.clear()
    return redirect(url_for("index_page_placeholder"))


@bp.route('/token_test', methods=["POST"])
def token_test():
    token = request.data.decode('utf8')
    return str(authorize(token))


def login_required(f):
    @functools.wraps(f)
    def wrapped_view(*args, **kwargs):
        token = request.headers.get('Authorization')
        if token is None or not authorize(token):
            return redirect(url_for("auth.login"))

        return f(*args, **kwargs)

    return wrapped_view
